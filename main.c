#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/* Get test sequences */
#include "test.h"

/**
 * main.c
 *
 * Run test sorting algorithms that are hilariously bad and broken.
 *
 * Compile with: gcc main.c -Ofast -o test
 * Or with RDRAND support: gcc main.c -Ofast -mrdrnd -o test
 **/

#define ARR_LEN(a) (sizeof(a) / sizeof(*a))

//#define RDRAND // Comment this if no RDRAND support

/* Seed random numbers */
int seed_random(uint16_t seed){
  #ifdef RDRAND
    /* Source: https://www.intel.com/content/www/us/en/developer/articles/guide/intel-digital-random-number-generator-drng-software-implementation-guide.html */
    unsigned char ok;
    asm volatile("rdseed %0; setc %1" : "=r" (seed), "=qm" (ok));
    return (int)ok;
  #else
    /* Use C implementation */
    srand(seed);
    return 0;
  #endif
}

/* Generate random number */
/* NOTE: It's possible we get bad random numbers, but we care more for speed
         here. */
int get_random(){
  #ifdef RDRAND
    /* Source: https://www.intel.com/content/www/us/en/developer/articles/guide/intel-digital-random-number-generator-drng-software-implementation-guide.html */
    uint16_t rand;
    unsigned char ok = 0;
    asm volatile("rdrand %0; setc %1" : "=r" (rand), "=qm" (ok));
    return rand;
  #else
    /* Use C implementation */
    return rand();
  #endif
}

/* Source: https://stackoverflow.com/a/51336144/2847743 */
int64_t currentTimeMillis(){
  struct timeval time;
  gettimeofday(&time, NULL);
  int64_t s1 = (int64_t)(time.tv_sec) * 1000;
  int64_t s2 = time.tv_usec / 1000;
  return s1 + s2;
}

int cmp_arr(int* a, int l, int* b, int n){
  if(l != n) return -1;
  for(int x = 0; x < l; x++) if(a[x] != b[x]) return -1;
  return 0;
}

void print_arr(int* a, int l){
  if(l <= 0) return;
  printf("{ %i", a[0]);
  for(int x = 1; x < l; x++) printf(", %i", a[x]);
  printf(" }");
}

/* Randomly swap, return when sorted */
void sort0(int* a, int l){
  seed_random(0);
  int ordered = 0;
  while(ordered == 0){
    /* Random element */
    int i = rand() % l;
    int j;
    /* Random element which is not what we already selected */
    while((j = get_random() % l) == i);
    /* Swap elements */
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
    /* Check if list now ordered */
    ordered = 1;
    for(int x = 1; x < l; ++x){
      if(a[x - 1] > a[x]){
        ordered = 0;
        break;
      }
    }
  }
}

/* Blind bubble sort */
void sort1(int* a, int l){
  /* Outer pass loop */
  for(int pass = 0; pass < l - 1; ++pass){
    /* Forward iterate through list */
    for(int x = 1; x < l - pass; ++x){
      /* Compare elements and swap */
      if(a[x - 1] > a[x]){
        /* Swap elements */
        int t = a[x - 1];
        a[x - 1] = a[x];
        a[x] = t;
      }
    }
  }
}

/* Unreasonably effective random sorting */
void sort2(int* a, int l){
  seed_random(0);
  int i = 0;
  while(i < l){
    /* Random element which is not what we already selected */
    int j = i + 1 + (get_random() % (l - (i + 1)));
    /* Swap elements */
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
    /* Check if list now ordered */
    i = 0;
    while(++i < l){
      if(a[i - 1] > a[i]){
        --i;
        break;
      }
    }
  }
}

/* Only randomly switch elements that improve the situation */
void sort3(int* a, int l){
  seed_random(0);
  int i = 0;
  while(i < l){
    int j;
    /* Random element in higher end which is smaller than what we want to swap */
    while(a[j = i + 1 + (get_random() % (l - (i + 1)))] > a[i]);
    /* Swap elements */
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
    /* Check if list now ordered */
    i = 0;
    while(++i < l){
      if(a[i - 1] > a[i]){
        --i;
        break;
      }
    }
  }
}

/* Deterministic search variant of sort3() */
void sort4(int* a, int l){
  int i = 0;
  while(i < l){
    int j = l;
    while(a[--j] >= a[i]);
    /* Swap elements */
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
    /* Check if list now ordered */
    i = 0;
    while(++i < l){
      if(a[i - 1] > a[i]){
        --i;
        break;
      }
    }
  }
}

struct node{ int val; struct node* left; struct node* right; };

void flatten(struct node* n, int* a, int* x){
  if(n == NULL) return;
  flatten(n->left, a, x);
  a[(*x)++] = n->val;
  flatten(n->right, a, x);
}

/* Binary sort tree */
void sort5(int* a, int l){
  struct node* nodes = malloc(sizeof(struct node) * l);
  int n = 0;
  struct node* root = &nodes[n++];
  root->val = a[0];
  root->left = NULL;
  root->right = NULL;
  for(int x = 1; x < l; x++){
    int v = a[x];
    struct node* parent = root;
    struct node* child = root;
    while(child != NULL){
      parent = child;
      if(a[x] < parent->val){
        child = parent->left;
      }else{
        child = parent->right;
      }
    }
    child = &nodes[n++];
    child->val = a[x];
    child->left = NULL;
    child->right = NULL;
    if(a[x] < parent->val){
      parent->left = child;
    }else{
      parent->right = child;
    }
  }
  int i = 0;
  flatten(root, a, &i);
  free(nodes);
}

void test(void (*f)(int*, int), char* name, int* t, int l, int* r, int n){
  int* a = malloc(l * sizeof(int));
  memcpy(a, t, l * sizeof(int));
  int64_t tb = currentTimeMillis();
  f(a, l);
  int64_t te = currentTimeMillis();
  printf("%i\t%s\t%s\t%li\n", l, name, cmp_arr(a, l, r, n) == 0 ? "pass" : "fail", te - tb);
  free(a);
}

void run_test(char* name, int* t, int l, int* r, int n){
  printf("Name: '%s'\n", name);
  if(l <= 12) test(sort0, "sort0", t, l, r, n);
  test(sort1, "sort1", t, l, r, n);
  if(l <= 2048) test(sort2, "sort2", t, l, r, n);
  test(sort3, "sort3", t, l, r, n);
  if(l <= 2048) test(sort4, "sort4", t, l, r, n);
  test(sort5, "sort5", t, l, r, n);
}

void gen_test(int size){
  int* a = malloc(size * sizeof(int));
  for(int x = 0; x < size; x++) a[x] = (int16_t)get_random();
  print_arr(a, size);
  printf("\n");
  sort1(a, size);
  print_arr(a, size);
  printf("\n");
  free(a);
}

int main(){
  run_test("0", t0, ARR_LEN(t0), r0, ARR_LEN(r0));
  run_test("1", t1, ARR_LEN(t1), r1, ARR_LEN(r1));
  run_test("2", t2, ARR_LEN(t2), r2, ARR_LEN(r2));
  run_test("3", t3, ARR_LEN(t3), r3, ARR_LEN(r3));
  run_test("4", t4, ARR_LEN(t4), r4, ARR_LEN(r4));
  run_test("5", t5, ARR_LEN(t5), r5, ARR_LEN(r5));
  run_test("6", t6, ARR_LEN(t6), r6, ARR_LEN(r6));
  run_test("7", t7, ARR_LEN(t7), r7, ARR_LEN(r7));
  run_test("8", t8, ARR_LEN(t8), r8, ARR_LEN(r8));
  run_test("9", t9, ARR_LEN(t9), r9, ARR_LEN(r9));
}
